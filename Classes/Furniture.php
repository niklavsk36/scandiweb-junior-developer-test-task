<?php
require_once 'classes/Product.php';

class Furniture extends Product {

    public function getGroupAttributeName() {
        return "Dimensions";
    }

    public function getUnitSeperator() {
        return " ";
    }
}
?>