<?php
require_once 'classes/Product.php';

class DvdDisc extends Product {

    public function getGroupAttributeName() {
        return "Size";
    }

    public function getUnitSeperator() {
        return " ";
    }
}
?>