<?php
require_once 'classes/Product.php';

class Book extends Product {

    public function getGroupAttributeName() {
        return "Weight";
    }

    public function getUnitSeperator() {
        return "";
    }
}
?>