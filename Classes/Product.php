<?php

class Product{
    protected $db;

    public function __construct(mysqli $db){
        $this->db = $db;
    }

    public function getProductList() {
        return $this->db->query('
        SELECT products.SKU, products.Name, products.Price,
                product_types.Name AS Type,
                type_attributes.Name AS AttributeName,
                GROUP_CONCAT(product_attributes.Value SEPARATOR \'x\') AS AttributeValue,
                type_attributes.Unit AS AttributeUnit
        FROM product_types JOIN type_attributes
            ON product_types.ID = type_attributes.TypeID
            JOIN products
            ON products.TypeID = product_types.ID
            JOIN product_attributes
            ON product_attributes.ProductID = products.SKU AND
            product_attributes.AttributeID = type_attributes.ID 
        GROUP BY product_attributes.ProductID
        ORDER BY products.SKU   
        ');
    }

    public function getTypeList() {
        return $this->db->query('
        SELECT * FROM product_types   
        ');
    }

    public function getTypeAttributeList() {
        return $this->db->query('
        SELECT product_types.ID AS TypeID,
                product_types.Name AS TypeName,
                type_attributes.ID AS AttributeID,
                type_attributes.Name AS AttributeName,
                type_attributes.Unit AS AttributeUnit
        FROM product_types JOIN type_attributes
            ON product_types.ID = type_attributes.TypeID
        ');
    }

    public function getOneTypeAttributeList($type) {
        return $this->db->query('
        SELECT * FROM type_attributes
	    WHERE TypeID = '.$type
        );
    }

    public function checkSku($SKU) {
        return $this->db->query('
        SELECT * FROM products
            WHERE SKU = '.'\''.$SKU.'\''
        );
    }

    public function addProduct($sku, $name, $price, $type, $attributes) {
        $sql = $this->db->prepare("INSERT INTO products (SKU, Name, Price, TypeID) VALUES (?, ?, ?, ?)");
        $sql->bind_param("ssdi", $sku, $name, $price, $type);
        $sql->execute();
        $sql->close();

        foreach ($attributes as $key => $item){
            $sql = $this->db->prepare("INSERT INTO product_attributes (ProductID, AttributeID, Value) VALUES (?, ?, ?)");
            $sql->bind_param("sid", $sku, $key, $item);
            $sql->execute();
            $sql->close();
        }
        return "index.php";
    }

    public function deleteProduct($id) {
        $sql = 'DELETE FROM product_attributes
        WHERE ProductID = '.'\''.$id.'\'';
        $this->db->prepare($sql)->execute();

        $sql = 'DELETE FROM products
        WHERE SKU = '.'\''.$id.'\'';
        $this->db->prepare($sql)->execute();
    }
}
?>