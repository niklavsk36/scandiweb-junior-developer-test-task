<?php
require_once 'database.php';
require_once 'classes/DvdDisc.php';
require_once 'classes/Book.php';
require_once 'classes/Furniture.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<!doctype HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="Styles/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Add</title>
    <script>
        $(document).ready(function() {
            $("select option[value='null']").prop('selected', true);
            $("input.product").val("");

            $("select").change(function() {
                var typeID = $("select").val();
                $(".attributes").hide();
                $("."+typeID).show();
            });
        });
    </script>
</head>
<body>
    <div class="container">
        <form action="storeProduct.php" method="post">
            <div class="row">
                <div class="col">
                    <h1>Product add</h1>
                </div>

                <div class="col">
                    <div class="row nav-row">
                        <input type="submit" value="Save" class="btn btn-secondary nav-btn">
                        <a href="index.php" class="btn btn-secondary nav-btn">Cancel</a>
                    </div>
                </div>

                <hr>
            </div>

            <div class="col-sm-5">
                <div class="row input-row">
                    <div class="col-sm-3">
                        <label for="sku">SKU</label>
                    </div>
                    <div class="col">
                        <input type="text" id="sku" name="sku" class="product">
                    </div>
                </div>

                <div class="row input-row">
                    <div class="col-sm-3">
                        <label for="name">Name</label>
                    </div>
                    <div class="col">
                        <input type="text" id="name" name="name" class="product">
                    </div>
                </div>

                <div class="row input-row">
                    <div class="col-sm-3">
                        <label for="price">Price ($)</label>
                    </div>
                    <div class="col">
                        <input type="text" id="price" name="price" class="product">
                    </div>
                </div>

                <div class="row input-row">
                    <div class="col-sm-5">
                        <label for="type">Type Switcher</label>
                    </div>
                    <div class="col">
                        <select name="type" class="product-type">
                            <option value="">Type Switcher</option>

                            <?php
                            $product = new Product($db);
                            $typeList = $product->getTypeList();

                            foreach ($typeList as $item): ?>
                                <option value="<?=$item['ID'] ?>"><?=$item['Name'] ?></option>
                            <?php endforeach; ?>

                        </select>
                    </div>
                </div>

                <?php
                $typeAttributeList = $product->getTypeAttributeList();
                foreach ($typeAttributeList as $item): ?>

                    <div class="row attributes input-row <?=$item['TypeID'] ?>" style="display: none">
                        <div class="col-sm-5">
                            <label for="<?=$item['AttributeID'] ?>"><?=$item['AttributeName']." (".$item['AttributeUnit'].")" ?></label>
                        </div>
                        <div class="col">
                            <input type="text" id="<?=$item['AttributeID'] ?>" name="<?=$item['AttributeID'] ?>" class="product">
                        </div>
                    </div>

                <?php endforeach;

                foreach ($typeList as $item):
                    $result = preg_replace("/[^a-zA-Z]+/", "", $item['Name']);
                    $result = ($result == "DVDdisc") ? "DvdDisc": $result;
                    $typeClass = new $result($db);
                    ?>

                    <div class="row attributes input-row <?=$item['ID'] ?>" style="display: none">
                        <div class="col">
                            <p>Please, provide <?= strtolower($typeClass->getGroupAttributeName()) ?>.</p>
                        </div>
                    </div>

                <?php endforeach; ?>

            </div>
        </form>

        <div class="col-sm-5">
            <?php if (isset($_SESSION['sku'])) { ?>
                <p class="alert alert-danger"><?=$_SESSION['sku'] ?></p>
            <?php } ?>

            <?php if (isset($_SESSION['name'])) { ?>
                <p class="alert alert-danger"><?=$_SESSION['name'] ?></p>
            <?php } ?>

            <?php if (isset($_SESSION['price'])) { ?>
                <p class="alert alert-danger"><?=$_SESSION['price'] ?></p>
            <?php } ?>

            <?php if (isset($_SESSION['type'])) { ?>
                <p class="alert alert-danger"><?=$_SESSION['type'] ?></p>
            <?php } ?>

            <?php
            foreach ($typeAttributeList as $item):
                if (isset($_SESSION['a'.$item['AttributeID']])) { ?>
                    <p class="alert alert-danger"><?=$_SESSION['a'.$item['AttributeID']] ?></p>
                <?php }
            endforeach; ?>
        </div>
    </div>
</body>
</html>

<?php
session_unset();
?>