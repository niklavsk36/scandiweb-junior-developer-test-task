-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Jan 28, 2021 at 06:37 PM
-- Server version: 8.0.18
-- PHP Version: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `juniortask`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `SKU` varchar(10) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Price` float NOT NULL,
  `TypeID` int(11) NOT NULL,
  PRIMARY KEY (`SKU`),
  KEY `TypeID` (`TypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`SKU`, `Name`, `Price`, `TypeID`) VALUES
('GGWP0007', 'War and Peace', 20, 2),
('HTA06484', 'Table', 60, 3),
('JVC200123', 'Acme DISC', 1, 1),
('PGJ97136', 'A Clockwork Orange', 13, 2),
('TR120555', 'Chair', 40, 3),
('YFR134872', 'Concert', 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

DROP TABLE IF EXISTS `product_attributes`;
CREATE TABLE IF NOT EXISTS `product_attributes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductID` varchar(10) NOT NULL,
  `AttributeID` int(11) NOT NULL,
  `Value` float NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ProductID` (`ProductID`),
  KEY `product_attributes_ibfk_1` (`AttributeID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`ID`, `ProductID`, `AttributeID`, `Value`) VALUES
(1, 'JVC200123', 1, 700),
(2, 'GGWP0007', 2, 2),
(3, 'TR120555', 3, 24),
(4, 'TR120555', 4, 45),
(5, 'TR120555', 5, 15),
(6, 'PGJ97136', 2, 1.5),
(7, 'YFR134872', 1, 1500),
(8, 'HTA06484', 3, 100),
(9, 'HTA06484', 4, 200),
(10, 'HTA06484', 5, 50);

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

DROP TABLE IF EXISTS `product_types`;
CREATE TABLE IF NOT EXISTS `product_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`ID`, `Name`) VALUES
(1, 'DVD-disc'),
(2, 'Book'),
(3, 'Furniture');

-- --------------------------------------------------------

--
-- Table structure for table `type_attributes`
--

DROP TABLE IF EXISTS `type_attributes`;
CREATE TABLE IF NOT EXISTS `type_attributes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TypeID` int(11) NOT NULL,
  `Name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Unit` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `TypeID` (`TypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `type_attributes`
--

INSERT INTO `type_attributes` (`ID`, `TypeID`, `Name`, `Unit`) VALUES
(1, 1, 'Size', 'MB'),
(2, 2, 'Weight', 'KG'),
(3, 3, 'Height', 'CM'),
(4, 3, 'Width', 'CM'),
(5, 3, 'Length', 'CM');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`TypeID`) REFERENCES `product_types` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD CONSTRAINT `product_attributes_ibfk_1` FOREIGN KEY (`AttributeID`) REFERENCES `type_attributes` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `product_attributes_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `products` (`SKU`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Constraints for table `type_attributes`
--
ALTER TABLE `type_attributes`
  ADD CONSTRAINT `type_attributes_ibfk_1` FOREIGN KEY (`TypeID`) REFERENCES `product_types` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
