<?php
require_once 'database.php';
require_once 'classes/DvdDisc.php';
require_once 'classes/Book.php';
require_once 'classes/Furniture.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
session_unset();
?>

<!doctype HTML>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="Styles/style.css">
    <title>List</title>
</head>

<body>
    <div class="container">
        <form action="deleteProduct.php" method="post">
            <div class="row">
                <div class="col">
                    <h1>Product list</h1>
                </div>

                <div class="col">
                    <div class="row nav-row">
                        <a href="addProduct.php" class="btn btn-secondary nav-btn">ADD</a>
                        <input type="submit" value="MASS DELETE" class="btn btn-secondary nav-btn">
                    </div>
                </div>
                <hr>
            </div>

            <div class="row">

            <?php
            $product = new Product($db);
            $productList = $product->getProductList();
            $itemCounter = 0;
            foreach ($productList as $item):
                $itemCounter++;
                if ($itemCounter == 5) {
                    $itemCounter = 1 ?>
            </div>
            <div class="row">
            <?php } ?>

            <div class="col-sm-3">
                <div class="product-box">
                    <input type="checkbox" id="<?= $item['SKU'] ?>" class="input-checkbox" name="<?= $item['SKU'] ?>" value="<?= $item['SKU'] ?>">
                    <?php
                    $result = preg_replace("/[^a-zA-Z]+/", "", $item['Type']);
                    $result = ($result == "DVDdisc") ? "DvdDisc": $result;
                    $typeClass = new $result($db);
                    ?>

                    <table class="text-center product-contents">
                        <tr><td><?= $item['SKU'] ?></td></tr>
                        <tr><td><?= $item['Name'] ?></td></tr>
                        <tr><td><?= number_format($item['Price'], 2, '.', '')." $" ?></td></tr>

                        <?php if ($item['AttributeName'] != $typeClass->getGroupAttributeName()) { ?>
                            <tr><td><?=$typeClass->getGroupAttributeName().": ".$item['AttributeValue'] ?></td></tr>
                        <?php } else { ?>
                            <tr><td><?=$item['AttributeName'].": ".$item['AttributeValue'].$typeClass->getUnitSeperator().$item['AttributeUnit'] ?></td></tr>
                        <?php } ?>

                    </table>
                </div>
            </div>
            <?php endforeach ?>
            </div>
        </form>
    </div>
</body>
</html>