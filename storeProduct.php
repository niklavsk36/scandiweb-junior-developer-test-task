<?php
require_once 'database.php';
require_once 'classes/DvdDisc.php';
require_once 'classes/Book.php';
require_once 'classes/Furniture.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

session_unset();

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$product = new Product($db);
$attributeList = $product->getOneTypeAttributeList($_POST['type']);
$error_check = false;

if (empty($_POST['sku'])) {
    $_SESSION['sku'] = "The SKU input field is required! ";
    $error_check = true;
}

if (empty($_POST['name'])) {
    $_SESSION['name'] = "The Name input field is required! ";
    $error_check = true;
}

if (empty($_POST['price'])) {
    $_SESSION['price'] = "The Price input field is required! ";
    $error_check = true;
}

if (empty($_POST['type'])) {
    $_SESSION['type'] = "The Type input field is required! ";
    $error_check = true;
} else {
    foreach ($attributeList as $item) {
        if (empty($_POST[$item['ID']])) {
            $_SESSION['a'.$item['ID']] = "The ".$item['Name']." input field is required! ";
            $error_check = true;
        }
    }
}

if (strlen($_POST['sku']) > 10) {
    if (!isset($_SESSION['sku'])) {
        $_SESSION['sku'] = "";
    }

    $_SESSION['sku'] = $_SESSION['sku']."The SKU input field cannot be longer than 10 characters! ";
    $error_check = true;
}

if (strlen($_POST['name']) > 30) {
    if (!isset($_SESSION['name'])) {
        $_SESSION['name'] = "";
    }

    $_SESSION['name'] = $_SESSION['name']."The Name input field cannot be longer than 30 characters! ";
    $error_check = true;
}

if ((!is_numeric($_POST['price']) || floatval($_POST['price']) < 0) && !empty($_POST['price'])) {
    if (!isset($_SESSION['price'])) {
        $_SESSION['price'] = "";
    }

    $_SESSION['price'] = $_SESSION['price']."The Price input field must be a valid number! ";
    $error_check = true;
}

if (!empty($_POST['type'])) {
    foreach ($attributeList as $item) {
        if ((!is_numeric($_POST[$item['ID']]) || floatval($_POST[$item['ID']]) < 0) && !empty($_POST[$item['ID']])) {
            if (!isset($_SESSION['a'.$item['ID']])) {
                $_SESSION['a'.$item['ID']] = "";
            }

            $_SESSION['a'.$item['ID']] = "The ".$item['Name']." input field must be a valid number! ";
            $error_check = true;
        }
    }
}

if (($product->checkSku($_POST['sku'])->num_rows) != 0 && !empty($_POST['sku'])) {
    if (!isset($_SESSION['sku'])) {
        $_SESSION['sku'] = "";
    }

    $_SESSION['sku'] = $_SESSION['sku']."The SKU input field must be unique! ";
    $error_check = true;
}

if ($error_check == true) {
    header('Location: addProduct.php');

} else {
    $attributes = [];

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $sku = test_input($_POST['sku']);
        $name = test_input($_POST['name']);
        $price = test_input($_POST['price']);
        $type = test_input($_POST['type']);

        foreach ($attributeList as $item) {
            $attributes[$item['ID']] = test_input($_POST[$item['ID']]);
        }

        header('Location:'. $product->addProduct($sku, $name, $price, $type, $attributes));
    } else {
        header('Location: index.php');
    }
}
?>
