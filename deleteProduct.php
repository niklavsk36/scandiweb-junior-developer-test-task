<?php
require_once 'database.php';
require_once 'classes/DvdDisc.php';
require_once 'classes/Book.php';
require_once 'classes/Furniture.php';

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$product = new Product($db);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    foreach ($_POST as $item){
        $id = test_input($item);
        $product->deleteProduct($id);
    }
}

header('Location: index.php');
?>
